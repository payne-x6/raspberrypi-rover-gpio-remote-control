/**
 * raspberrysocket.h
 * Author: Jan Hak
 * Class that recieve and send messages from/to client. Each client is represented by instance of this class
 **/
#ifndef RASPBERRYSOCKET_H
#define RASPBERRYSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QByteArray>
#include <QDataStream>

#include <iostream>

// Definition of message types
#define END_BYTE 0

#define GET_NEW_GPIO 1
#define GET_GPIO_TYPE 2
#define GET_GPIO_VALUE 3
#define SET_GPIO_TYPE 4
#define SET_GPIO_VALUE 5

#define MODULE_MOTOR_CONTROLLER_NEW 64
#define MODULE_MOTOR_CONTROLLER_DIRECTION_VECTOR 65

#define TOOL_GET_SETTINGS 128
#define TOOL_SHUTDOWN 129
#define TOOL_PING 130
#define TOOL_PONG 131


class RaspberrySocket : public QObject
{
    Q_OBJECT
private:
    // Pointer on parent class, that menage communication over network with client
    QTcpSocket *socket;

    /**
     * Parse informations about new GPIO pin
     * @param Stream that stores rest of still unparsed bytes of packet
     **/
    void readNewGPIO(QDataStream &);
    /**
     * Parse informations about new type of GPIO pin that should be set in GUI
     * @param Stream that stores rest of still unparsed bytes of packet
     **/
    void readGetGPIOType(QDataStream &);
    /**
     * Parse informations about new value set on GPIO pin that should be shown in GUI
     * @param Stream that stores rest of still unparsed bytes of packet
     **/
    void readGetGPIOValue(QDataStream &);
    /**
     * Parse informations about new motor controller
     * @param Stream that stores rest of still unparsed bytes of packet
     **/
    void readNewMotorController(QDataStream &);
    /**
     * Parse message ping recieved from client
     * @param Stream that stores rest of still unparsed bytes of packet
     **/
    void readPing(QDataStream &);
public:
    /**
     * Creates new interface of communication over QTCPSocket
     * @param parent object
     * @param parent object that manage comunication over network. Should be (don't have to be) same as parent object
     **/
    explicit RaspberrySocket(QObject *parent = 0, QTcpSocket *socket = 0);
    /**
     * Return pointer on parent object that manage comunication over network
     * @return Pointer on parent object
     **/
    QTcpSocket *getSocket();

signals:
    void destroyed(RaspberrySocket *socket);

    void newCommonGPIO(quint8);
    void GPIOTypeChange(quint8, quint8);
    void GPIOValueChange(quint8, quint8);
    void newMotorController(quint8);

    void messageUnresolved();
public slots:
    void onClose();
    void read();

    void sendSetGPIOType(quint8, quint8);
    void sendSetGPIOValue(quint8, quint8);

    void sendMotorControllerDirection(quint8, quint16, quint8);

    void sendGetSettings();
    void sendShutdown();
    void sendPing();
    void sendPong();
};

#endif // RASPBERRYSOCKET_H
