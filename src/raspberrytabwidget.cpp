/**
 * raspberrytabwidget.cpp
 * Author: Jan Hak
 */
#include "raspberrytabwidget.h"

RaspberryTabWidget::RaspberryTabWidget(QWidget *parent, const QString &name, RaspberrySocket *socket) :
    QWidget(parent),
    layout(this),
    tabs(this),
    name(name),
    gpioSettingsWidget(this),
    controllerWidget(this)

{
    this->socket = socket;

    connect(socket, SIGNAL(destroyed(RaspberrySocket*)), this, SLOT(ended()));
    connect(&gpioSettingsWidget, SIGNAL(sendChangeGPIOTypeSignal(quint8,quint8)), socket, SLOT(sendSetGPIOType(quint8, quint8)));
    connect(&gpioSettingsWidget, SIGNAL(sendChangeGPIOValueSignal(quint8,quint8)), socket, SLOT(sendSetGPIOValue(quint8,quint8)));
    connect(&controllerWidget, SIGNAL(sendDirectionVectorSignal(quint8,quint16,quint8)), socket, SLOT(sendMotorControllerDirection(quint8,quint16,quint8)));
    tabs.addTab(&gpioSettingsWidget, "Settings");
    tabs.addTab(&controllerWidget, "Controller");

    layout.addWidget(&(this->name));
    layout.addWidget(&tabs);
}

QString RaspberryTabWidget::getName() {
    return name.text();
}


void RaspberryTabWidget::ended() {
    emit remove(this);
}

void RaspberryTabWidget::createNewGPIO(quint8 id) {
    gpioSettingsWidget.createGPIOWidget(id);
    controllerWidget.addGPIOIndicator(id);
}

void RaspberryTabWidget::getGPIOTypeChange(quint8 id, quint8 type)  {
    gpioSettingsWidget.setGPIOType(id, type);
    controllerWidget.setGPIOType(id, type);
}

void RaspberryTabWidget::getGPIOValueChange(quint8 id, quint8 value)  {
    gpioSettingsWidget.setGPIOValue(id, value);
    controllerWidget.setGPIOValue(id, value);
}

void RaspberryTabWidget::createNewMotorController(quint8 id) {
    controllerWidget.addController(id);
}

RaspberrySocket &RaspberryTabWidget::getSocket() {
    return *socket;
}
