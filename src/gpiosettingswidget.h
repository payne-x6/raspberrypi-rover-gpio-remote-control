/**
 * gpiosettingswidget.h
 * Author: Jan Hak
 * Widget that store scrollarea and show list of GPIO pins, that are able to manualy setup
 */
#ifndef GPIOSETTINGSWIDGET_H
#define GPIOSETTINGSWIDGET_H

#include "gpiopinwidget.h"

#include <QWidget>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QLabel>

#include <iostream>

using namespace std;

class GpioSettingsWidget : public QWidget
{
    Q_OBJECT
private:
    // Layout of widget
    QVBoxLayout rootLayout;
    // ScrollArea of widget
    QScrollArea scrollArea;

    // Widget stored in scroll area
    QWidget list;
    // Layout of widget stored in scroll area
    QVBoxLayout root;
public:
    explicit GpioSettingsWidget(QWidget *parent = 0);
    /**
     * Add new GPIO pin widget to the list
     * @param pointer on GPIOPinWidget
     **/
    void addGPIOPin(GPIOPinWidget *widget);
    /**
     * Set type of GPIO pin widget that's picked by ID
     * @param ID of wanted GPIO pin
     * @param type that should be GPIO pin set
     **/
    void setGPIOType(quint8, quint8);
    /**
     * Set value of GPIO pin widget that's picked by ID
     * @param ID of wanted GPIO pin
     * @param type that should be set on GPIO pin
     **/
    void setGPIOValue(quint8, quint8);
signals:
    void setGPIOTypeSignal(quint8, quint8);
    void setGPIOValueSignal(quint8, quint8);
    void sendChangeGPIOTypeSignal(quint8, quint8);
    void sendChangeGPIOValueSignal(quint8, quint8);
public slots:
    void createGPIOWidget(quint8);
    void sendChangeGPIOTypeSlot(quint8, quint8);
    void sendChangeGPIOValueSlot(quint8, quint8);
};

#endif // GPIOSETTINGSWIDGET_H
