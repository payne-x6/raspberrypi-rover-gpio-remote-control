/**
 * raspberrytcpserver.h
 * Author: Jan Hak
 * RaspberryTcpServer manage sockets for communication with clients signed into server
 */
#ifndef RASPBERRYTCPSERVER_H
#define RASPBERRYTCPSERVER_H

#include "raspberrysocket.h"

#include <QTcpServer>
#include <QTcpSocket>
#include <QHostAddress>
#include <QVector>


using namespace std;

class RaspberryTcpServer : public QTcpServer
{
    Q_OBJECT
private:
    // Address of host
    QHostAddress addr;
    // Port of host
    quint16 port;

    // Vector that stores opened sockets
    QVector<RaspberrySocket *> openSockets;

public:
    /**
     * Create new instacne of RaspberryTcpServer
     * @param Parent object
     * @param Address where should server listen. Default is every address.
     * @param Port where server should listen. Default it pick first idle.
     **/
    explicit RaspberryTcpServer(QObject *parent = 0, const QHostAddress &addr = QHostAddress::Any, const quint16 port = 0);
    /**
     * Starts listening of server on network socket
     * @return Able to start listening flag
     **/
    bool start();
    /**
     * Stop network communication with clients
     **/
    void stop();
signals:
    void newSocket(RaspberrySocket *socket = 0);
public slots:
    void createSocket();
    void removeSocket(RaspberrySocket *socket= 0);
    
};

#endif // RASPBERRYTCPSERVER_H
