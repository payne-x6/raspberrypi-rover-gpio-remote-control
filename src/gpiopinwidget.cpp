/**
 * gpiowidget.cpp
 * Author: Jan Hak
 */
#include "gpiopinwidget.h"

GPIOPinWidget::GPIOPinWidget(QWidget *parent, quint8 id) :
    QWidget(parent),
    root(this),
    typeComboBox(this),
    pinStateButton("LOW", this)
{
    this->id = id;
    setMinimumWidth(100);

    root.addWidget(new QLabel(QString::number(id), this));

    // Setup ComboBox for setting type of GPIO pin
    QStringList items;
    items << "OUTPUT" << "INPUT";
    typeComboBox.addItems(items);
    root.addWidget(&typeComboBox);
    connect(&typeComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(typeChange(QString)));

    // Setup Button for change value on output GPIO pin
    root.addWidget(&pinStateButton);
    connect(&pinStateButton, SIGNAL(clicked()), this, SLOT(valueChange()));
}

void GPIOPinWidget::setType(quint8 type) {
    typeComboBox.setCurrentIndex(type ? TYPE_INPUT : TYPE_OUTPUT);
    pinStateButton.setDisabled(type ? true : false);
}

void GPIOPinWidget::setGPIOTypeSlot(quint8 id, quint8 type) {
    if(this->id == id) {
        setType(type);
    }
}

void GPIOPinWidget::setGPIOValueSlot(quint8 id, quint8 value) {
    if(this->id == id) {
        pinStateButton.setText(value ? "HIGH" : "LOW");
    }
}

void GPIOPinWidget::typeChange(QString type) {
    if(type == "OUTPUT") {
        pinStateButton.setEnabled(true);
        emit sendChangeType(id, TYPE_OUTPUT);
    }
    else if(type == "INPUT") {
        pinStateButton.setEnabled(false);
        emit sendChangeType(id, TYPE_INPUT);
    }
}

void GPIOPinWidget::valueChange() {
    if(pinStateButton.text() == "HIGH") {
        pinStateButton.setText("LOW");
    }
    else {
        pinStateButton.setText("HIGH");
    }
    emit sendChangeValue(id, (pinStateButton.text() == "HIGH") ? 1 : 0);
}
