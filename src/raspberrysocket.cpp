/**
 * raspberrysocket.cpp
 * Author: Jan Hak
 */
#include "raspberrysocket.h"

RaspberrySocket::RaspberrySocket(QObject *parent, QTcpSocket *socket) :
    QObject(parent)
{
    this->socket = socket;

    connect(socket, SIGNAL(disconnected()), this, SLOT(onClose()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(read()));
}

QTcpSocket *RaspberrySocket::getSocket() {
    return socket;
}

void RaspberrySocket::onClose() {
    emit destroyed(this);
}

void RaspberrySocket::read() {
    QDataStream buffer(socket->readAll());

    quint8 type;

    // While any data, take first byte of stream and then parse rest of message
    while(!buffer.atEnd()) {
        buffer >> type;
        switch(type) {
        case GET_NEW_GPIO:
            readNewGPIO(buffer);
            break;
        case GET_GPIO_TYPE:
            readGetGPIOType(buffer);
            break;
        case GET_GPIO_VALUE:
            readGetGPIOValue(buffer);
            break;
        case MODULE_MOTOR_CONTROLLER_NEW:
            readNewMotorController(buffer);
            break;
        case TOOL_PING:
            readPing(buffer);
            break;
        default:
            emit messageUnresolved();
            break;
        }
    }
}

void RaspberrySocket::readNewGPIO(QDataStream &stream) {
    quint8 id, endByte;
    stream >> id >> endByte;
    if(!endByte) {
        emit newCommonGPIO(id);
    }
    else {
        emit messageUnresolved();
    }
}

void RaspberrySocket::readGetGPIOType(QDataStream &stream) {
    quint8 id, type, endByte;

    stream >> id >> type >> endByte;
    if(!endByte) {
        emit GPIOTypeChange(id, type);
    }
    else {
        emit messageUnresolved();
    }
}

void RaspberrySocket::readGetGPIOValue(QDataStream &stream) {
    quint8 id, value, endByte;
    stream >> id >> value >> endByte;
    if(!endByte) {
        emit GPIOValueChange(id, value);
    }
    else {
        emit messageUnresolved();
    }
}

void RaspberrySocket::readNewMotorController(QDataStream &stream) {
    quint8 id, endByte;
    stream >> id >> endByte;
    if(!endByte) {
        emit newMotorController(id);
    }
    else {
        emit messageUnresolved();
    }
}

void RaspberrySocket::readPing(QDataStream &stream) {
    quint8 endByte;
    stream >> endByte;
    if(!endByte) {
        QMetaObject::invokeMethod(this, "sendPong");
    }
    else {
        emit messageUnresolved();
    }
}

void RaspberrySocket::sendSetGPIOType(quint8 id, quint8 type) {
    QByteArray bytes;
    QDataStream data(&bytes, QIODevice::WriteOnly);

    data << quint8(SET_GPIO_TYPE) << quint8(id) << quint8(type) << quint8(END_BYTE);

    socket->write(bytes);
}

void RaspberrySocket::sendSetGPIOValue(quint8 id, quint8 value) {
    QByteArray bytes;
    QDataStream data(&bytes, QIODevice::WriteOnly);

    data << quint8(SET_GPIO_VALUE) << quint8(id) << quint8(value) << quint8(END_BYTE);

    socket->write(bytes);
}

void RaspberrySocket::sendMotorControllerDirection(quint8 moduleId, quint16 angle, quint8 speed) {
    QByteArray bytes;
    QDataStream data(&bytes, QIODevice::WriteOnly);

    data << quint8(MODULE_MOTOR_CONTROLLER_DIRECTION_VECTOR) << quint8(moduleId) << quint16(angle) << quint8(speed) << quint8(END_BYTE);

    socket->write(bytes);
}

void RaspberrySocket::sendGetSettings() {
    QByteArray bytes;
    QDataStream data(&bytes, QIODevice::WriteOnly);

    data << quint8(TOOL_GET_SETTINGS) << quint8(END_BYTE);
    socket->write(bytes);
}

void RaspberrySocket::sendShutdown() {
    QByteArray bytes;
    QDataStream data(&bytes, QIODevice::WriteOnly);

    data << quint8(TOOL_SHUTDOWN) << quint8(END_BYTE);

    socket->write(bytes);
}

void RaspberrySocket::sendPing() {
    QByteArray bytes;
    QDataStream data(&bytes, QIODevice::WriteOnly);

    data << quint8(TOOL_PING) << quint8(END_BYTE);

    socket->write(bytes);
}

void RaspberrySocket::sendPong() {
    QByteArray bytes;
    QDataStream data(&bytes, QIODevice::WriteOnly);

    data << quint8(TOOL_PONG) << quint8(END_BYTE);

    socket->write(bytes);
}



