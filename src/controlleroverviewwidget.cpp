/**
 * controlleroverviewwidget.h
 * Author: Jan Hak
 */

#include "controlleroverviewwidget.h"

ControllerOverviewWidget::ControllerOverviewWidget(QWidget *parent) :
    QWidget(parent),
    layout(this),
    area(this),
    areaWidget(&area),
    areaLayout(&areaWidget),
    header(&areaWidget)
{

    area.setWidgetResizable(true);

    area.setWidget(&areaWidget);

    areaLayout.addWidget(&header);
    layout.addWidget(&area);
}

void ControllerOverviewWidget::addGPIOIndicator(quint8 id) {
    header.addPin(id);
}

void ControllerOverviewWidget::setGPIOType(quint8 id, quint8 type) {
    header.setGPIOType(id, type);
}

void ControllerOverviewWidget::setGPIOValue(quint8 id, quint8 value) {
    header.setGPIOValue(id, value);
}

void ControllerOverviewWidget::addController(quint8 id) {
    ControllerWidget *controller = new ControllerWidget(&areaWidget, id);
    connect(controller, SIGNAL(sendDirectionVector(quint8, quint16, quint8)),
            this, SLOT(sendDirectionVectorSlot(quint8, quint16, quint8)));
    areaLayout.addWidget(controller);
}

void ControllerOverviewWidget::sendDirectionVectorSlot(quint8 id, quint16 angle, quint8 speed) {
    emit sendDirectionVectorSignal(id, angle, speed);
}
