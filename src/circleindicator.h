/**
 * circleindicator.h
 * Author: Jan Hak
 *
 * Draw circle that represents GPIO pin and its setting
 */
#ifndef CircleIndicator_H
#define CircleIndicator_H

#include <QWidget>
#include <QPainter>
#include <QRadialGradient>

class CircleIndicator : public QWidget
{
    Q_OBJECT
private:
    // ID of pin that represents
    quint8 id;

    // Size of icon
    quint8 width;
    quint8 height;

    // Type of pin
    quint8 type;
    // Value of pin
    quint8 value;
public:
    /**
     * Create new widget with pin indicator
     *
     * @param Pointer on parent widget
     * @param Id of GPIO pin
     * @param Width of icon
     * @param Height of icon
     **/
    explicit CircleIndicator(QWidget *parent = 0, quint8 id = -1, quint8 width = 10, quint8 height = 10);
    /**
     * Set type of indicator
     * @param Indicator of type (0 = output, 1 = input)
     **/
    void setType(quint8 type);
    /**
     * Set value of indicator
     * @param Indicator value. 0 for LOW else HIGH
     **/
    void setValue(quint8 value);
    void paintEvent(QPaintEvent *);
signals:
    
public slots:
    void changeType(quint8, quint8);
    void changeValue(quint8, quint8);
    
};

#endif // CircleIndicator_H
