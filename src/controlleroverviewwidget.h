/**
 * controlleroverviewwidget.h
 * Author: Jan Hak
 *
 * Widget where are stored all widget that should be in Controller tab
 */
#ifndef CONTROLLEROVERVIEWWIDGET_H
#define CONTROLLEROVERVIEWWIDGET_H

#include "controllerwidget.h"
#include "headerwidget.h"

#include <QWidget>
#include <QScrollArea>
#include <QHBoxLayout>
#include <QVBoxLayout>

class ControllerOverviewWidget : public QWidget
{
    Q_OBJECT
private:
    // Layout of widget
    QVBoxLayout layout;
    // Scroll area of widget
    QScrollArea area;
    // Widget stored in scroll area
    QWidget areaWidget;
    // Layout of widget stored in scroll area
    QHBoxLayout areaLayout;
    // Header widget that show all GPIO indicators and is in every ControllerOverviewWidget
    HeaderWidget header;
public:
    explicit ControllerOverviewWidget(QWidget *parent = 0);
    /**
     * Add GPIO indicator into header widget
     * @param ID of pin
     **/
    void addGPIOIndicator(quint8);
    /**
     * Set type of GPIO indicator
     * @param ID of GPIO indicator
     * @param New type for GPIO indicator
     **/
    void setGPIOType(quint8, quint8);
    /**
     * Set value of GPIO indicator
     * @param ID of GPIO indicator
     * @param New value set on GPIO indicator
     **/
    void setGPIOValue(quint8, quint8);

    /**
     * Create and show new motor controller widget
     * @param ID of new motor controller
     **/
    void addController(quint8);
signals:
    void sendDirectionVectorSignal(quint8, quint16, quint8);
public slots:
    void sendDirectionVectorSlot(quint8, quint16, quint8);
    
};

#endif // CONTROLLEROVERVIEWWIDGET_H
