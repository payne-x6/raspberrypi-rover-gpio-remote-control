/**
 * headerwidget.h
 * Author: Jan Hak
 * Header widget stores circle indicator widgets in Controller tab, to show actual settings of GPIO pins
 */
#ifndef HEADERWIDGET_H
#define HEADERWIDGET_H

#include "circleindicator.h"

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QString>

class HeaderWidget : public QWidget
{
    Q_OBJECT
private:
    // Layout of header
    QGridLayout layout;
    // Separator is empty widget with setup minimal size, to make space in header
    QWidget separator;

    // Initialize legend that is part of every header
    void initLegend();
public:
    explicit HeaderWidget(QWidget *parent = 0);
    /**
     * Add new GPIO pin indicator
     * @param ID of GPIO pin that indicator represents
     **/
    void addPin(quint8);
    /**
     * Setup GPIO indicator type
     * @param ID of GPIO pin
     * @param new type of GPIO indicator
     **/
    void setGPIOType(quint8, quint8);
    /**
     * Setup GPIO indicator value
     * @param ID of GPIO pin
     * @param new value of GPIO indicator
     **/
    void setGPIOValue(quint8, quint8);
signals:
    void changeType(quint8, quint8);
    void changeValue(quint8, quint8);
    
public slots:
    
};

#endif // HEADERWIDGET_H
