/**
 * controllerwidget.cpp
 * Author: Jan Hak
 */ 
#include "controllerwidget.h"

ControllerWidget::ControllerWidget(QWidget *parent, quint8 id) :
    QWidget(parent)
{
    this->id = id;

    pointX = 0;
    pointY = 0;

    setMinimumSize(DIAMETER, DIAMETER);
}

double ControllerWidget::getAngle() {
    // Count angle between [0,0] and [x,y] and rotate 90° becouse angle 0 should be on North
    double angle = qAtan2(pointY, pointX) + M_PI / 2.;
    // Move to positive values
    if(angle < 0) {
        angle += 2 * M_PI;
    }
    return ((2 * M_PI) - angle);
}

double ControllerWidget::getSpeed() {
    // Pythagoras distance
    double distance = qSqrt((pointX * pointX) + (pointY * pointY));
    return (distance > (DIAMETER / 2.)) ? 1. : (distance / (DIAMETER / 2.));
}

void ControllerWidget::paintEvent(QPaintEvent *) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    // Translate [0, 0] to center of widget
    painter.translate(width() / 2, height() / 2);

    // Draw elipses
    for (int diameter = 0; diameter <= DIAMETER; diameter += 32) {
            painter.setPen(QPen(QColor(0, diameter / 2, 127, 255), 3));
            painter.drawEllipse(QRectF(-diameter / 2.0, -diameter / 2.0,
                                       diameter, diameter));
    }

    // Draw primary axis
    painter.setPen(Qt::black);
    painter.drawLine(0, -((DIAMETER/2) + 10), 0, ((DIAMETER/2)+ 10));
    painter.drawLine(-((DIAMETER/2) + 10), 0, ((DIAMETER/2) + 10), 0);
    // Draw secondary axis
    painter.setPen(Qt::DashLine);
    painter.drawLine(-(DIAMETER/2), -(DIAMETER/2), (DIAMETER/2), (DIAMETER/2));
    painter.drawLine((DIAMETER/2), -(DIAMETER/2), -(DIAMETER/2), (DIAMETER/2));

    // Draw controlling point
    painter.setBrush(Qt::red);
    painter.setPen(Qt::transparent);
    painter.drawEllipse(QRectF(pointX - 5, pointY - 5, 10, 10));
}

void ControllerWidget::mouseMoveEvent(QMouseEvent *event) {
    double tmpX = event->x() - (width() / 2.);
    double tmpY = event->y() - (height() / 2.);
    double distance = qSqrt((tmpX * tmpX) + (tmpY * tmpY));

    // Keep dot in max range
    if(distance > 128) {
        pointX = ((tmpX / distance) * (DIAMETER / 2.));
        pointY = ((tmpY / distance) * (DIAMETER / 2.));
    }
    else {
        pointX = tmpX;
        pointY = tmpY;
    }
    // Redraw
    update();
    emit sendDirectionVector(id, quint16(getAngle() * 180 / M_PI), quint8(getSpeed() * 100));
}


