/**
 * main.cpp
 * Author: Jan Hak
 * Main file of application
 */
#include "raspberrytcpserver.h"
#include "window.h"

#include <QApplication>

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    // Create and show window
    Window win;
    win.show();

    // Wait for close window (application) and then return output value of app
    return a.exec();
}
