/**
 * circleindicator.cpp
 * Author: Jan Hak
 */

#include "circleindicator.h"

CircleIndicator::CircleIndicator(QWidget *parent, quint8 id, quint8 width, quint8 height) :
    QWidget(parent)
{
    this->id = id;

    this->width = width;
    this->height = height;

    this->type = 0;
    this->value = 0;

    setMinimumSize(width * 2, height * 2);
}

void CircleIndicator::paintEvent(QPaintEvent *ev) {
    QPainter paint(this);

    //Set gradient color of fill by type and value
    QRadialGradient gradient(QPointF(width * 3. / 2., (this->size().height() - height) / 2.), width);
    if(type == 0) {
        gradient.setColorAt(0, QColor(0, 0, 255));
        gradient.setColorAt(1, (value) ? QColor(0, 0, 128) : QColor(255, 255, 255));
    }
    else if(type == 1) {
        gradient.setColorAt(0, QColor(0, 255, 0));
        gradient.setColorAt(1, (value) ? QColor(0, 128, 0) : QColor(255, 255, 255));
    }
    
    // Setup brush and paint
    QBrush brush(gradient);
    paint.setBrush(brush);
    paint.drawEllipse(width / 2., (this->size().height() - height) / 2., width, height);
}

void CircleIndicator::setType(quint8 type) {
    this->type = type;
    update();
}

void CircleIndicator::setValue(quint8 value) {
    this->value = value;
    update();
}


void CircleIndicator::changeType(quint8 id, quint8 type) {
    if(this->id == id) {
        setType(type);
    }
}

void CircleIndicator::changeValue(quint8 id, quint8 value) {
    if(this->id == id) {
        setValue(value);
    }
}
