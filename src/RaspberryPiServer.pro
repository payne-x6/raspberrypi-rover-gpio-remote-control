QT += core gui network widgets

SOURCES += \
    main.cpp \
    raspberrytcpserver.cpp \
    window.cpp \
    raspberrysocket.cpp \
    raspberrytabwidget.cpp \
    gpiosettingswidget.cpp \
    gpiopinwidget.cpp \
    controllerwidget.cpp \
    controlleroverviewwidget.cpp \
    headerwidget.cpp \
    circleindicator.cpp

HEADERS += \
    raspberrytcpserver.h \
    window.h \
    raspberrysocket.h \
    raspberrytabwidget.h \
    gpiosettingswidget.h \
    gpiopinwidget.h \
    controllerwidget.h \
    controlleroverviewwidget.h \
    headerwidget.h \
    circleindicator.h

RESOURCES = resources.qrc
