/**
 * controllerwidget.h
 * Author: Jan Hak
 * Controller widget show circle that count from position of mouse controlled dot angle and speed and send it to client
 **/
#ifndef CONTROLLERWIDGET_H
#define CONTROLLERWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QMouseEvent>
#include <QtCore/qmath.h>

#include <iostream>

#define DIAMETER 256

using namespace std;

class ControllerWidget : public QWidget
{
    Q_OBJECT
private:
    // ID of motor controller module
    quint8 id;
    // Position od mouse controlled dot
    float pointX, pointY;
public:
    /** 
     * Create new instance of ControllerWidget
     * @param pointer on parent widget
     * @param ID of MotorController module 
     **/
    explicit ControllerWidget(QWidget *parent = 0, quint8 id = -1);
    /**
     * Count and return angle of movement
     * @return Angle in radians
     **/
    double getAngle();
    /**
     * Count and return speed of movement
     * @return Speed in interval <0, 1>
     **/
    double getSpeed();
protected:
    void paintEvent(QPaintEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
signals:
    void sendDirectionVector(quint8, quint16, quint8);
public slots:

};

#endif // CONTROLLERWIDGET_H
