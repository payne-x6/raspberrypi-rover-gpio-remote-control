/**
 * gpiopinwidget.h
 * Author: Jan Hak
 * GPIOPinWidget has elements that allow you setup GPIO pin remotely from GUI
 */
#ifndef GPIOPINWIDGET_H
#define GPIOPINWIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>

#include <iostream>

#define TYPE_OUTPUT 0
#define TYPE_INPUT 1

class GPIOPinWidget : public QWidget
{
    Q_OBJECT
private:
    // Combobox for picking type
    QComboBox typeComboBox;
    // Button for change state of output pin
    QPushButton pinStateButton;
    // Layout of widget
    QHBoxLayout root;
    // ID of GPIO pin
    quint8 id;

    /**
     * Set type of GPIOPinWidget, that means change picked value of typeComboBox
     * @param Identificator of pin
     **/
    void setType(quint8);
public:
    /**
     * Create new instance of widget
     * @param parent widget
     * @param ID of pin
     **/
    explicit GPIOPinWidget(QWidget *parent = 0, quint8 id = -1);
signals:
    void sendChangeType(quint8, quint8);
    void sendChangeValue(quint8, quint8);
    
public slots:
    void setGPIOTypeSlot(quint8, quint8);
    void setGPIOValueSlot(quint8, quint8);

    void typeChange(QString);
    void valueChange();
    
};

#endif // GPIOPINWIDGET_H
