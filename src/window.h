/**
 * window.h
 * Author: Jan Hak
 * Main window of whole application
 */
#ifndef WINDOW_H
#define WINDOW_H

#include "raspberrytcpserver.h"
#include "raspberrytabwidget.h"

#include <QMainWindow>
#include <QToolBar>
#include <QToolButton>
#include <QAction>
#include <QStackedWidget>
#include <QInputDialog>
#include <QMessageBox>


class Window : public QMainWindow
{
    Q_OBJECT
private:
    // Instance of RaspberryTcpServer
    RaspberryTcpServer server;

    // Toolbar of window
    QToolBar toolbar;
    // Red (STOPED) icon of Start button
    QIcon startIcon;
    // Green (RUNNING) icon of Start button
    QIcon stopIcon;
    // Start button in toolbar panel
    QAction startAction;
    // Previous client button in toolbar panel
    QAction previousAction;
    // Next client button in toolbar panel
    QAction nextAction;
    // Shutdown client button in toolbar panel
    QAction shutdownAction;

    // Widget that stores panel of all clients and show always only one of them
    QStackedWidget screenStack;
    // Index of active client panel that is active at the moment
    qint8 screenIndex;

    // Initialize toolbar
    void initToolbar();

    // Setup right settings of buttons by actual settings of window
    void updateButtons();
public:
    explicit Window(QWidget *parent = 0);
    
signals:
    
public slots:
    void startButtonClicked();
    void newScreen(RaspberrySocket *);
    void removeScreen(RaspberryTabWidget *);
    void previousScreen();
    void nextScreen();
    void shutdownBoard();
};

#endif // WINDOW_H
