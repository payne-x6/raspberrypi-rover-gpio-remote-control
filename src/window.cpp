/**
 * window.cpp
 * Author: Jan Hak
 */
#include "window.h"

Window::Window(QWidget *parent) :
    QMainWindow(parent),
    server(this, QHostAddress::Any, 31415),
    toolbar(this),
    startIcon(":/graphics/indicator_inactive.xpm"),
    stopIcon(":/graphics/indicator_active.xpm"),
    startAction(startIcon, "Start server", &toolbar),
    previousAction(QIcon(":/graphics/left.xpm"), "Previous Raspberry window", &toolbar),
    nextAction(QIcon(":/graphics/right.xpm"), "Next Raspberry window", &toolbar),
    shutdownAction(QIcon(":/graphics/shutdown.xpm"), "Shutdown Raspberry", &toolbar),
    screenStack(this)
{
    setMinimumSize(200, 200);

    // Initialize toolbar and Central widget
    initToolbar();
    setCentralWidget(&screenStack);

    screenIndex = 0;

    connect(&server, SIGNAL(newSocket(RaspberrySocket *)), this, SLOT(newScreen(RaspberrySocket *)));
}

void Window::initToolbar() {
    toolbar.setIconSize(QSize(35, 35));

    connect(&startAction, SIGNAL(triggered()), this, SLOT(startButtonClicked()));
    connect(&previousAction, SIGNAL(triggered()), this, SLOT(previousScreen()));
    connect(&nextAction, SIGNAL(triggered()), this, SLOT(nextScreen()));
    connect(&shutdownAction, SIGNAL(triggered()), this, SLOT(shutdownBoard()));

    toolbar.addAction(&startAction);
    toolbar.addSeparator();
    toolbar.addAction(&previousAction);
    toolbar.addAction(&nextAction);
    toolbar.addSeparator();
    toolbar.addAction(&shutdownAction);

    addToolBar(&toolbar);
    updateButtons();
}

void Window::updateButtons() {
    // Screen stack has some widgets
    if(screenStack.count()) {
        // Enable shutdown of that client
        shutdownAction.setEnabled(true);
        //When widget deleted, update index
        if(screenStack.count() <= screenIndex) {
            screenIndex = screenStack.count() - 1;
            screenStack.setCurrentIndex(screenIndex);
        }
        // Previous and next button disabled?
        previousAction.setDisabled(screenIndex < 1);
        nextAction.setDisabled(screenStack.count() - 1 <= screenIndex);
    }
    else {
        // Disable buttons
        screenIndex = 0;
        shutdownAction.setDisabled(true);
        previousAction.setDisabled(true);
        nextAction.setDisabled(true);
    }

}

void Window::startButtonClicked() {
    // If server running - stop
    if(server.isListening()) {
        server.stop();
        startAction.setIcon(startIcon);
        startAction.setToolTip("Start server");
    }
    // If server not running - try start
    else {
        // Started
        if(server.start()) {
            startAction.setIcon(stopIcon);
            startAction.setToolTip("Stop server");
        }
        // Didn't started - show alert
        else {
            QMessageBox::information(this, "Starting error", "Unnable to create server. Maybe other server is already running.");
        }
    }
}

void Window::newScreen(RaspberrySocket *socket) {
    bool ok;
    // Show dialog for add new client
    QString name = QInputDialog::getText(this, "New client is trying to connect",
            "Do you allow connect of this device?\nInsert name:", QLineEdit::Normal,
            socket->getSocket()->peerAddress().toString(), &ok);
    // Dialog OK - invite client
    if(ok) {
        RaspberryTabWidget *tab = new RaspberryTabWidget(&screenStack, name, socket);
        connect(tab, SIGNAL(remove(RaspberryTabWidget *)), this, SLOT(removeScreen(RaspberryTabWidget *)));
        connect(socket, SIGNAL(newCommonGPIO(quint8)), tab, SLOT(createNewGPIO(quint8)));
        connect(socket, SIGNAL(GPIOTypeChange(quint8,quint8)), tab, SLOT(getGPIOTypeChange(quint8, quint8)));
        connect(socket, SIGNAL(GPIOValueChange(quint8,quint8)), tab, SLOT(getGPIOValueChange(quint8, quint8)));
        connect(socket, SIGNAL(newMotorController(quint8)), tab, SLOT(createNewMotorController(quint8)));

        // Send slot sendGetSettings to client
        QMetaObject::invokeMethod(socket, "sendGetSettings");

        screenStack.addWidget(tab);
        updateButtons();
    }
    // Dialog Cancled - close client
    else {
        socket->getSocket()->close();
    }
}

void Window::removeScreen(RaspberryTabWidget *widget) {
    screenStack.removeWidget(widget);
    updateButtons();
    QMessageBox::information(this, "Client disconnected", QString("Client '%1' were disconnected").arg(widget->getName()));
}

void Window::previousScreen() {
    screenIndex--;
    screenStack.setCurrentIndex(screenIndex);

    updateButtons();
}

void Window::nextScreen() {
    screenIndex++;
    screenStack.setCurrentIndex(screenIndex);

    updateButtons();
}


void Window::shutdownBoard() {
    RaspberryTabWidget *widget = dynamic_cast<RaspberryTabWidget *>(screenStack.currentWidget());
    if(widget) {
        // Send slot sendShutdown to client
        QMetaObject::invokeMethod(&(widget->getSocket()), "sendShutdown");
    }
}
