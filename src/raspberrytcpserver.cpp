/**
 * raspberrytcpserver.cpp
 * Author: Jan Hak
 */
#include "raspberrytcpserver.h"

RaspberryTcpServer::RaspberryTcpServer(QObject *parent, const QHostAddress &addr, const quint16 port) :
    QTcpServer(parent)
{
    this->addr = addr;
    this->port = port;

    connect(this, SIGNAL(newConnection()), this, SLOT(createSocket()));
}

void RaspberryTcpServer::createSocket() {
    QTcpSocket *s = nextPendingConnection();
    RaspberrySocket *socket = new RaspberrySocket(s, s);

    connect(socket, SIGNAL(destroyed(RaspberrySocket *)), this, SLOT(removeSocket(RaspberrySocket *)));
    openSockets.append(socket);

    emit newSocket(socket);
}

bool RaspberryTcpServer::start() {
    return listen(addr, port);
}

void RaspberryTcpServer::stop() {
    QVectorIterator<RaspberrySocket *> it(openSockets);
    // Stop communication with all clients
    while(it.hasNext()) {
        it.next()->getSocket()->close();
    }
    close();
}

void RaspberryTcpServer::removeSocket(RaspberrySocket *socket) {
    openSockets.remove(openSockets.indexOf(socket));
}
