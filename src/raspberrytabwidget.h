/**
 * raspberrytabwidget.h
 * Author: Jan Hak
 * Widget that represents window of each network client
 */
#ifndef RASPBERRYTABWIDGET_H
#define RASPBERRYTABWIDGET_H

#include "raspberrysocket.h"
#include "gpiosettingswidget.h"
#include "controlleroverviewwidget.h"

#include <QTabWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <iostream>

class RaspberryTabWidget : public QWidget
{
    Q_OBJECT
private:
    // Pointer on socket for communication with client
    RaspberrySocket *socket;
    // Layout of widget
    QVBoxLayout layout;
    // TabWidget of tabs for setting or controll client 
    QTabWidget tabs;
    // Label with name of client
    QLabel name;
    // Settings tab
    GpioSettingsWidget gpioSettingsWidget;
    // Controll tab
    ControllerOverviewWidget controllerWidget;
public:
    /**
     * Create new instance of widget
     * @param Parent widget of this one
     * @param Name of client
     * @param Socket for communication with client
     **/
    explicit RaspberryTabWidget(QWidget *parent = 0, const QString &name = "Unnamed", RaspberrySocket *socket = 0);
    /**
     * Return reference on communication socket
     * @return Reference on RaspberrySocket
     **/
    RaspberrySocket &getSocket();
    /**
     * Return name of client
     * @return Name of client
     **/
    QString getName();
signals:
    void remove(RaspberryTabWidget *);
public slots:
    void ended();

    void createNewGPIO(quint8);
    void getGPIOTypeChange(quint8, quint8);
    void getGPIOValueChange(quint8, quint8);
    void createNewMotorController(quint8);
};

#endif // RASPBERRYTABWIDGET_H
