/**
 * headerwidget.cpp
 * Author: Jan Hak
 */
#include "headerwidget.h"

HeaderWidget::HeaderWidget(QWidget *parent) :
    QWidget(parent),
    layout(this),
    separator(this)
{
    initLegend();
}

void HeaderWidget::initLegend() {
    separator.setMinimumHeight(20);

    // Add label of legend
    QLabel *legendLabel = new QLabel("Legend:", this);
    legendLabel->setStyleSheet("font-weight: bold");

    // Create 4 circle indicators, for 4 possible settings, with default id -1
    CircleIndicator *i1 = new CircleIndicator(this);
    i1->setType(0);
    i1->setValue(0);
    CircleIndicator *i2 = new CircleIndicator(this);
    i2->setType(0);
    i2->setValue(1);
    CircleIndicator *i3 = new CircleIndicator(this);
    i3->setType(1);
    i3->setValue(0);
    CircleIndicator *i4 = new CircleIndicator(this);
    i4->setType(1);
    i4->setValue(1);

    // Add to layout
    layout.addWidget(legendLabel, 0, 0);
    layout.addWidget(new QLabel("LOW", this), 0, 1);
    layout.addWidget(new QLabel("HIGH", this), 0, 2);

    layout.addWidget(new QLabel("OUTPUT", this), 1, 0);
    layout.addWidget(i1, 1, 1);
    layout.addWidget(i2, 1, 2);

    layout.addWidget(new QLabel("INPUT", this), 2, 0);
    layout.addWidget(i3, 2, 1);
    layout.addWidget(i4, 2, 2);

    layout.addWidget(&separator, 3, 0);
}

void HeaderWidget::addPin(quint8 id) {
    layout.addWidget(new QLabel(QString::number(id), this),id + 4, 0);

    CircleIndicator *indicator = new CircleIndicator(this, id);
    connect(this, SIGNAL(changeType(quint8, quint8)), indicator, SLOT(changeType(quint8, quint8)));
    connect(this, SIGNAL(changeValue(quint8, quint8)), indicator, SLOT(changeValue(quint8, quint8)));
    layout.addWidget(indicator, id + 4, 1);
}

void HeaderWidget::setGPIOType(quint8 id, quint8 type) {
    emit changeType(id, type);
}

void HeaderWidget::setGPIOValue(quint8 id, quint8 value) {
    emit changeValue(id, value);
}

