/**
 * gpiosettingswidget.cpp
 * Author: Jan Hak
 */
#include "gpiosettingswidget.h"

GpioSettingsWidget::GpioSettingsWidget(QWidget *parent) :
    QWidget(parent),
    rootLayout(this),
    scrollArea(this),
    list(&scrollArea),
    root(&list)
{
    root.setAlignment(Qt::AlignCenter);

    scrollArea.setWidgetResizable(true);
    scrollArea.setWidget(&list);
    rootLayout.addWidget(&scrollArea);
}

void GpioSettingsWidget::createGPIOWidget(quint8 id) {
    GPIOPinWidget *widget = new GPIOPinWidget(&scrollArea, id);
    addGPIOPin(widget);
}

void GpioSettingsWidget::addGPIOPin(GPIOPinWidget *widget) {
    root.addWidget(widget);
    scrollArea.setWidget(&list);

    connect(this, SIGNAL(setGPIOTypeSignal(quint8, quint8)), widget, SLOT(setGPIOTypeSlot(quint8, quint8)));
    connect(this, SIGNAL(setGPIOValueSignal(quint8, quint8)), widget, SLOT(setGPIOValueSlot(quint8, quint8)));

    connect(widget, SIGNAL(sendChangeType(quint8,quint8)), this, SLOT(sendChangeGPIOTypeSlot(quint8, quint8)));
    connect(widget, SIGNAL(sendChangeValue(quint8,quint8)), this, SLOT(sendChangeGPIOValueSlot(quint8, quint8)));

}

void GpioSettingsWidget::setGPIOType(quint8 id, quint8 type) {
    emit setGPIOTypeSignal(id, type);
}

void GpioSettingsWidget::setGPIOValue(quint8 id, quint8 value) {
    emit setGPIOValueSignal(id, value);
}

void GpioSettingsWidget::sendChangeGPIOTypeSlot(quint8 id, quint8 type) {
    emit sendChangeGPIOTypeSignal(id, type);
}

void GpioSettingsWidget::sendChangeGPIOValueSlot(quint8 id, quint8 value) {
    emit sendChangeGPIOValueSignal(id, value);
}
